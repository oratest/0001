node('ob-docker1'){

    print "The entered url is "+ params.URL
    print "Downloading the artifact: "
    sh"""
    wget --quiet "${params.URL}" --no-proxy
    ls
    """
        
    String[] s = params.URL.split('/com/lbg/')
    String[] s1 = params.URL.split('/')
    s[0] = 'https://ob-sc-raw-repo-banking.dropbox.sbx.zone/repository/ob-sc-raw-repo/com/lbg/'
    String new_url = s[0] + s[1]
            
    print "The generated dropbox nexus url is "+ new_url
            
    print "Uploading the artifact " + s1[-1] + " to nexus dropbox"
    withCredentials([
        usernameColonPassword(	credentialsId: 'poc18.obairlock',
        variable: 'NEXUS_AIRLOCK_CREDS')
    ]){
        sh"""
        curl --noproxy '*' -u $NEXUS_AIRLOCK_CREDS --upload-file "${s1[-1]}" "${new_url}"
        """
    }
                
    print "cleaning up the workspace"
    sh'''
    cd /apps/jenkins2ws/workspace/OpenBanking/Upload-to-dropbox
    rm -rf *
    '''
}